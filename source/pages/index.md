---
permalink: /
title: subversive software
description: I create and improve websites for organisations working towards justice.
layout: index
---

## why

Many organisations are doing great and important work against imperialism, racism, patriarchy, and other forms of oppression. However, their impact is limited by their ability to share knowledge and skills, attract and retain members, and communicate effectively. I create and improve websites that solve these problems and empower organisations in their fight for justice.

## what

I can make great websites, and make your existing website even better. Read [what I can do for your website]({% link _solutions/websites.md %}).

## who

I'm a developer, designer, and activist dedicated to the struggle for justice. Read more about me in [my CV]({% link _people/sam.md %}).
