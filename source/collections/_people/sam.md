---
title: sam lubbers
description: "I'm a developer, designer, and activist dedicated to the struggle for justice."
name: sam lubbers
pronouns: he/him
permalink: /cv
---
## summary

I have studied computer science, but my development goes way beyond my formal education: Through books, conversations, and activism I develop a critical consciousness about structural oppressions and how to combat them. I also continuously develop my skills in programming, design, writing, and cybersecurity, as these skills are crucial in my work towards justice. 

## work

### Extinction Rebellion Netherlands (Amsterdam | September 2019 - present)

At Extinction Rebellion I have lead various projects as part of their national tech team. I have improved their website by coding templates and enhancing the WordPress CMS, administered and optimized their CRM system for contacting and engaging activists, lead the development of an open source volunteer platform, wrote a digital security guide, and created an application to periodically back up important data.

In addition to these technical contributions, at different periods, I served in these roles: representative, to collaborate with other groups in the movement; facilitator, to make the meeting agenda and facilitate the meeting; integrator, to welcome and integrate new people; and internal coordinator, to organise tasks and improve documentation.

### Metabolic (Amsterdam | October 2020 - October 2021)

During my one year of work at Metabolic I worked on various software services that assisted sustainability consultants in making organisations more sustainable. Most of my work revolved around cleaning datasets of environmental impact data, storing that data in relational PostgreSQL databases, and creating APIs that made that data available to people and other applications. I did all this work in Python and SQL.

### Evolution AI (London | January 2018 - June 2018)

During this internship, I coded the backend and frontend of a natural language processing platform. I coded the backend with Python, SQLAlchemy and the Flask web framework, and the frontend with HTML, CSS, JavaScript and Vue.js.

## education

### Bachelor of Science in Computer Science with Business at the University of Bath (Bath, UK | September 2015 - June 2019)

In my bachelor's, I learned to program with Java and C, gained a deep understanding of algorithms, parallel computing and the architecture of computers, and I advanced my knowledge of probability, set theory and logic. I graduated with first-class honours with an average grade of 74,5.

For my final year thesis, I implemented and evaluated various machine learning models that identified land use in satellite images of the Amazon Rainforest, with the purpose of using such models for reducing deforestation.

### self-education

These are some books from which I have learned valuable skills and developed a more critical consciousness.

#### skills

- programming: The Pragmatic Programmer; Effective Python.
- website design: The Elements Of The User Experience; Don't Make me Think About It.
- writing: Elements Of Style; The Sense Of Style; Stylish Academic Writing.

#### critical consciousness

- imperialism: How Europe Underdeveloped Africa; Open Veins of Latin America; Border and Rule.
- racism: The New Jim Crow; White Fragility; How to Be an Antiracist; What White People Can Do Next; Me And White Supremacy.
- feminism: The Will To Change; Feminism For The 99%; Living A Feminist Life; Hood Feminism.
- climate crisis: This Changes Everything; The Great Derangement; The Nutmeg's Curse.
- other: Sacred Economics; The Age Of Surveillance Capitalism; Pedagogy Of The Oppressed; The Shock Doctrine; The Red Deal; Understanding Power.

## skills and qualities

### software skills

- programming languages and frameworks: Python, HTML, CSS, JavaScript, SQL, Vue.js, PHP, Lua.
- software tools: Linux, Git, (Neo)Vim, Ansible.
- content management systems and website generators: WordPress, Jekyll.  

### other skills

- proficient writing.
- website (UI/UX) design.
- project management.
- languages: English (native), Spanish (native), Dutch (near native), Catalan (intermediate). 

### personal qualities

- proactive.
- fast learner.
- work well independently and in a team.
- great at communicating with technical and non-technical people.
- active listener.
- focused.
