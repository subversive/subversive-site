---
title: websites
description: "I create great websites from start to end: I understand objectives, design an accessible user experience, write clear texts, and program fast and secure code."
benefits:
  title: "I create websites that are:"
  list:
    - usable
    - accessible
    - fast
    - sustainable
    - secure
    - beautiful
services:
  title: "What I can do"
  list:
    - title: "understand"
      description: "the objectives and struggles of the people who use and own the website, so that I create what is most needed."
    - title: "design"
      description: "the user experience: I structure information across and within pages, provide useful navigation, and craft the graphic design."
    - title: "write"
      description: "website texts with a clear and consistent tone and voice."
    - title: "code"
      description: "the website, configure content managament systems (CMS) like WordPress, and deploy the finished website to a server."
---
